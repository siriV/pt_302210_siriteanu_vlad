package ClientQueues;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.Comparator;

public class Client implements Comparable{
	
	private AtomicInteger ID;
	private AtomicInteger arrivalTime;
	private AtomicInteger serviceTime;
	
	public Client(AtomicInteger ID, AtomicInteger arrivalTime, AtomicInteger serviceTime) 
	{
		
		this.ID = ID;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}
	
	
	
	public AtomicInteger getServiceTime()
	{
		return this.serviceTime;
	}
	
	public AtomicInteger getArrivalTime()
	{
		return this.arrivalTime;
	}

	@Override
	public String toString() 
	{
		return "(" + ID.toString() + "," + arrivalTime.toString() + "," + serviceTime.toString() + ") ";
	}

	public int compareTo(Object o) {
		Client client = (Client)o;
		
		return this.arrivalTime.intValue() - client.arrivalTime.intValue();
	}
	
	

}
