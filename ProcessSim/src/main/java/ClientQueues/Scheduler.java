package ClientQueues;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Scheduler{
	
	private AtomicInteger numberOfQueues;
	private AtomicInteger numberOfClients;
	private List<Queue> queues;
	private List<Thread> threads;
	
	public Scheduler(AtomicInteger numberOfQueues, AtomicInteger numberOfClients)
	{
		this.numberOfQueues = numberOfQueues;
		this.numberOfClients = numberOfClients;
		this.queues = new ArrayList<Queue>(this.numberOfQueues.intValue());
		this.threads = new ArrayList<Thread>(this.numberOfQueues.intValue());
		
		for(int i = 0; i < this.numberOfQueues.intValue(); i++)
		{
			queues.add(new Queue(numberOfClients));
			threads.add(new Thread(queues.get(i)));
			threads.get(i).start();
		}
		
	}
	
	private Integer minWaitingTimeQueue()
	{
		Integer waitingTime = Integer.MAX_VALUE;
		Integer minTimeQueue = 0;
		for(int i = 0; i < this.numberOfQueues.intValue(); i++)
		{
			if(queues.get(i).getWaitingTime() < waitingTime)
			{
				waitingTime = queues.get(i).getWaitingTime();
				minTimeQueue = i;
			}
			if(waitingTime == 0)
			{
				break;
			}
		}
		
		return minTimeQueue;
	}
	
	public void dispatchClient(Client client)
	{
		Integer minTimeQueue = minWaitingTimeQueue();
		queues.get(minTimeQueue).addClient(client);
		if(queues.get(minTimeQueue).getState() == true)
		{
			queues.get(minTimeQueue).startThread();
			threads.set(minTimeQueue, new Thread(queues.get(minTimeQueue)));
			threads.get(minTimeQueue).start();
		}
	}
	
	public void killThreads()
	{
		for(Queue q : queues)
		{
			q.killThread();
		}
	}
	
	public Integer getMaxQueueWait()
	{
		Integer maxWait = Integer.MIN_VALUE;
		for(Queue q : queues)
		{
			maxWait = Math.max(maxWait, q.getWaitingTime());
		}
		
		return maxWait;
	}

	@Override
	public String toString() 
	{
		String result = "";
		for(Integer i = 0; i < this.numberOfQueues.intValue(); i++)
		{
			
			result += "Queue " + i.toString() + ": " + queues.get(i).toString() + "\n";
		}
		return result;
	}
	
	

}
