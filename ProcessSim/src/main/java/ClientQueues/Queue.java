package ClientQueues;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue implements Runnable{
	
	private BlockingQueue<Client> clients;
	private AtomicInteger waitingTime;
	private volatile boolean killRunningThread;
	
	public Queue(AtomicInteger numberOfClients) 
	{
		
		this.clients = new LinkedBlockingQueue<Client>(numberOfClients.intValue());
		this.waitingTime = new AtomicInteger(0);
	}
	
	public void addClient(Client client)
	{
		
		try{this.clients.add(client);}catch(Exception ex) {}
		waitingTime.addAndGet(client.getServiceTime().intValue());
	}
	
	public Integer getWaitingTime()
	{
		return this.waitingTime.intValue();
	}
	
	public BlockingQueue<Client> getClients()
	{
		return this.clients;
	}
	
	public void startThread()
	{
		this.killRunningThread = false;
	}
	
	public void killThread()
	{
		this.killRunningThread = true;
	}
	
	public boolean getState()
	{
		return this.killRunningThread;
	}
	
	private String buildResult()
	{
		String result;
		if(waitingTime.intValue() == 0 || clients.peek() == null || clients.peek().getServiceTime().intValue() == 0)
		{
			result = "closed";
		}
		else
		{
			result = clients.toString();
		}
		return result;
	}

	public void run() 
	{
		while(!this.killRunningThread)
		{	
			while(clients.peek() != null)
			{
				try{Thread.sleep(1000);}catch(Exception ex) {}
				
				waitingTime.decrementAndGet();
				if(clients.peek().getServiceTime().decrementAndGet() == 0)
				{
					clients.poll();
				}
			}
			
			killThread();
		}
	}
	
	public String toString()
	{
		String res = buildResult();
		return res;
		
	}
	
}
