package PT2020.T2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import ClientQueues.*;
import Utility.FileWork;

public class Simulator implements Runnable, FileWork{
	
	private AtomicInteger nrClients;
	private AtomicInteger nrQueues;
	private AtomicInteger simulationTime;
	private AtomicInteger minArrival;
	private AtomicInteger maxArrival;
	private AtomicInteger minService;
	private AtomicInteger maxService;
	private AtomicInteger avgWaitingTime;
	private File outFile;
	
	private  Scheduler scheduler;
	private  List<Client> client;
	
	private void generateClients()
	{
		client = new ArrayList<Client>(this.nrClients.intValue());
		for(int i = 0; i < this.nrClients.intValue(); i++)
		{
			AtomicInteger clientID = new AtomicInteger(i);
			AtomicInteger clientArrivalTime = new AtomicInteger((int)(Math.random() * ((maxArrival.intValue() - minArrival.intValue()) + 1) + minArrival.intValue()));
			AtomicInteger clientServiceTime = new AtomicInteger((int)(Math.random() * ((maxService.intValue() - minService.intValue()) + 1) + minService.intValue()));
			
			client.add(new Client(clientID, clientArrivalTime, clientServiceTime));
		}
		
		Collections.sort(client);
	}
	
	public Simulator(File file, File outFile) 
	{
		List<String> fileResult;
		try
		{
			fileResult = readFromFile(file, "[1-9]{1}[0-9]*");
			this.nrClients = new AtomicInteger(Integer.parseInt(fileResult.get(0)));
			this.nrQueues = new AtomicInteger(Integer.parseInt(fileResult.get(1)));
			this.simulationTime = new AtomicInteger(Integer.parseInt(fileResult.get(2)));
			this.minArrival = new AtomicInteger(Integer.parseInt(fileResult.get(3)));
			this.maxArrival = new AtomicInteger(Integer.parseInt(fileResult.get(4)));
			this.minService = new AtomicInteger(Integer.parseInt(fileResult.get(5)));
			this.maxService = new AtomicInteger(Integer.parseInt(fileResult.get(6)));
			this.avgWaitingTime = new AtomicInteger(0);
			this.outFile = outFile;
			try
			{
				this.outFile.createNewFile();
			}
			catch(Exception ex) {System.out.println("File does not exists and could not be created ");return;}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			return;
		}
		
		generateClients();
		scheduler = new Scheduler(nrQueues, nrClients);		
		
	}
	
	private String getResult(Integer currentTime)
	{
		String result = "Waiting clients: " + client.toString() + "\n";
		result += "Time: " +currentTime.toString() + "\n";
		result += scheduler.toString() + "\n";
		
		return result;
	}
	
	private Integer getMaxWait(Integer maxWait)
	{
		if(client.isEmpty() && maxWait <= 0)
		{
			maxWait = scheduler.getMaxQueueWait();
		}
		else
		{
			maxWait--;
		}
		
		return maxWait;
	}
	
	public void run() 
	{
		FileWriter fWriter;
		
		try{fWriter =  new FileWriter(this.outFile.toString());}catch (Exception ex){System.out.println(ex.getMessage());return;}
		Integer currentTime = 0;
		Integer maxWait = 0;
		Integer nrProcessedClients = 0;
		while(currentTime.intValue() < simulationTime.intValue() && (!client.isEmpty() || maxWait > 0))
		{
			while(!client.isEmpty() && client.get(0).getArrivalTime().intValue() == currentTime.intValue())
			{
				scheduler.dispatchClient(client.get(0));
				if(currentTime.intValue() + client.get(0).getServiceTime().intValue() < simulationTime.intValue())
				{
					nrProcessedClients++;
					avgWaitingTime.addAndGet(-client.get(0).getArrivalTime().intValue() + client.get(0).getServiceTime().intValue() + currentTime.intValue());
				}
				client.remove(0);
			}
			String result = getResult(currentTime);
			try{fWriter.write(result);}catch(Exception ex) {}
			maxWait = getMaxWait(maxWait);
			currentTime++;
			try{Thread.sleep(1000);}catch(Exception ex) {}
			
		}
		scheduler.killThreads();
		try{fWriter.write("Average waiting time: " + avgWaitingTime.intValue() / nrProcessedClients);}catch(Exception ex) {System.out.println(ex.getMessage());}
		try{fWriter.close();}catch(Exception ex) {}
	}
	
}
