package PT2020.T2;

import java.io.File;
import java.util.Scanner;

public class App 
{
    public static void main( String[] args )
    {
    	
        try{
        Simulator sim = new Simulator(new File(args[0]), new File(args[1]));
        Thread simThread = new Thread(sim);
        simThread.start();
        try{simThread.join();}catch(Exception ex) {}
        }
        catch(Exception ex){System.out.println(ex.getMessage());}
    }
}
